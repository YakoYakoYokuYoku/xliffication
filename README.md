# XliffIcation

## Open Source XLIFF translations applier

Xliffication is an XLIFF translation applier for text documents that outputs all their approved translations accourdingly.
It provides an API for both parsing XLIFF documents and replacement of strings, alongside a command-line interface.

## Installation

Simply build the project using `cargo`, locate the output binary and place it somewhere else in the system `PATH`.

```
cargo build
```

## Demos

Examples of the tool's usage are found in the `tests` folder.
