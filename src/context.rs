use quick_xml::events::{BytesStart, Event};
use quick_xml::Reader;

use std::borrow::Cow;
use std::io::{self, BufReader, Read};

use crate::error;

#[derive(Clone, Copy, Debug)]
pub enum ContextType {
    SourceFile,
    LineNumber,
    Invalid,
}

impl ContextType {
    #[must_use]
    pub fn from_bytes(b: &[u8]) -> ContextType {
        match b {
            b"sourcefile" => ContextType::SourceFile,
            b"linenumber" => ContextType::LineNumber,
            _ => ContextType::Invalid,
        }
    }
}

#[derive(Clone, Debug)]
pub struct ContextGroup<'a> {
    name: String,
    purpose: String,
    ctx_type: ContextType,
    sourcefile: Cow<'a, [u8]>,
    pub line_number: u32,
}

impl<'a> ContextGroup<'a> {
    #[must_use]
    pub fn new() -> Self {
        ContextGroup {
            name: String::new(),
            purpose: String::new(),
            ctx_type: ContextType::Invalid,
            sourcefile: Cow::default(),
            line_number: 0,
        }
    }

    #[must_use]
    pub fn sourcefile(&self) -> Cow<'a, [u8]> {
        self.sourcefile.clone()
    }

    /// An XLIFF context group parser from a XML event reader.
    ///
    /// Consumes a `quick_xml::Reader` with a `quick_xml::BytesStart` and returns a `ContextGroup`.
    ///
    /// # Errors
    ///
    /// Returns `xliffication::error::Error`s on invalid XML input.
    pub fn parse_from_entity<R: Read>(
        reader: &mut Reader<BufReader<R>>,
        e: &BytesStart,
    ) -> Result<Self, error::Error> {
        let mut ctx_group = ContextGroup::new();
        let mut buf = Vec::new();

        for attr in e.attributes() {
            match attr {
                Ok(a) => match a.key {
                    b"name" => {
                        ctx_group.name = String::from_utf8_lossy(a.value.as_ref()).to_string();
                    }
                    b"purpose" => {
                        ctx_group.purpose = String::from_utf8_lossy(a.value.as_ref()).to_string();
                    }
                    _ => (),
                },
                Err(e) => {
                    eprintln!("couldn't parse attribute in test.xlf: {}", e);
                    return Err(error::Error::from(e));
                }
            }
        }
        loop {
            match reader.read_event(&mut buf) {
                Ok(Event::Start(ref e)) => {
                    if let b"context" = e.name() {
                        for attr in e.attributes() {
                            match attr {
                                Ok(a) => {
                                    if let b"context-type" = a.key {
                                        ctx_group.ctx_type =
                                            ContextType::from_bytes(a.value.as_ref());
                                    }
                                }
                                Err(e) => {
                                    eprintln!("couldn't parse attribute in test.xlf: {}", e);
                                    return Err(error::Error::from(e));
                                }
                            }
                        }
                        let mut text = Vec::new();
                        match ctx_group.ctx_type {
                            ContextType::SourceFile => {
                                ctx_group.sourcefile =
                                    Cow::Owned(Vec::from(reader.read_text(e.name(), &mut text)?));
                            }
                            ContextType::LineNumber => {
                                ctx_group.line_number =
                                    match reader.read_text(e.name(), &mut text)?.parse() {
                                        Ok(id) => id,
                                        Err(_) => {
                                            return Err(error::Error::from(io::Error::from(
                                                io::ErrorKind::InvalidInput,
                                            )))
                                        }
                                    }
                            }
                            ContextType::Invalid => {
                                return Err(error::Error::XliffError(String::from(
                                    "Invalid context-type for context",
                                )))
                            }
                        }
                    }
                }
                Ok(Event::End(ref e)) => {
                    if let b"context-group" = e.name() {
                        break;
                    }
                }
                Ok(Event::Eof) => {
                    return Err(error::Error::Io(io::Error::from(
                        io::ErrorKind::UnexpectedEof,
                    )))
                }
                Err(e) => {
                    eprintln!("couldn't open test.xlf: {}", e);
                    return Err(error::Error::from(e));
                }
                _ => (),
            }
        }

        Ok(ctx_group)
    }
}

impl<'a> Default for ContextGroup<'a> {
    fn default() -> Self {
        ContextGroup::new()
    }
}
