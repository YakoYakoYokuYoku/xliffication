use quick_xml::events::Event;
use quick_xml::Reader;

use std::borrow::Cow;
use std::io::{self, BufReader, Read};

use crate::error;
use crate::transunit::TransUnit;

#[derive(Clone, Copy, Debug)]
pub enum XliffMajorVersion {
    XliffMajorV1,
    XliffMajorV2,
    Invalid,
}

#[derive(Clone, Copy, Debug)]
pub enum XliffVersion {
    XliffV1_0,
    XliffV1_1,
    XliffV1_2,
    XliffV2_0,
    XliffV2_1,
    Invalid,
}

#[derive(Clone, Debug)]
pub struct XliffDoc<'a> {
    major: XliffMajorVersion,
    version: XliffVersion,
    source: Cow<'a, [u8]>,
    target: Cow<'a, [u8]>,
    body: Vec<TransUnit<'a>>,
}

impl<'a> XliffDoc<'a> {
    #[must_use]
    pub fn new() -> Self {
        XliffDoc {
            major: XliffMajorVersion::Invalid,
            version: XliffVersion::Invalid,
            source: Cow::default(),
            target: Cow::default(),
            body: Vec::new(),
        }
    }

    #[must_use]
    pub fn source(&'a self) -> &'a [u8] {
        &*self.source
    }

    #[must_use]
    pub fn target(&'a self) -> &'a [u8] {
        &*self.target
    }

    #[must_use]
    pub fn body(self) -> Vec<TransUnit<'a>> {
        self.body
    }

    pub fn borrowed_source(&mut self, text: &'a [u8]) {
        self.source = Cow::Borrowed(text);
    }

    pub fn borrowed_target(&mut self, text: &'a [u8]) {
        self.target = Cow::Borrowed(text);
    }

    pub fn owned_source(&mut self, text: Vec<u8>) {
        self.source = Cow::Owned(text);
    }

    pub fn owned_target(&mut self, text: Vec<u8>) {
        self.target = Cow::Owned(text);
    }

    /// An XLIFF version parser from an attribute value.
    ///
    /// Borrows a mutable `quick_xml::Reader` and modifies the `XliffDoc`
    /// accourdingly.
    ///
    /// # Errors
    ///
    /// Returns `xliffication::error::Error`s on invalid XML input.
    pub fn parse_version(&mut self, value: &[u8]) -> Result<(), error::Error> {
        match value[0] {
            b'1' => self.major = XliffMajorVersion::XliffMajorV1,
            b'2' => self.major = XliffMajorVersion::XliffMajorV2,
            _ => {
                return Err(error::Error::XliffError(String::from(
                    "Invalid XLIFF version",
                )))
            }
        }
        match value {
            b"1.0" => self.version = XliffVersion::XliffV1_0,
            b"1.1" => self.version = XliffVersion::XliffV1_1,
            b"1.2" => self.version = XliffVersion::XliffV1_2,
            b"2.0" => self.version = XliffVersion::XliffV2_0,
            b"2.1" => self.version = XliffVersion::XliffV2_1,
            _ => {
                return Err(error::Error::XliffError(String::from(
                    "Invalid XLIFF version",
                )))
            }
        }
        Ok(())
    }

    /// An XLIFF translation units parser from a XML event reader.
    ///
    /// Borrows a mutable `quick_xml::Reader` and modifies the `XliffDoc`
    /// accourdingly.
    ///
    /// # Errors
    ///
    /// Returns `xliffication::error::Error`s on invalid XML input.
    pub fn parse_translation_units<R: Read>(
        &mut self,
        reader: &mut Reader<BufReader<R>>,
        buf: &mut Vec<u8>,
    ) -> Result<(), error::Error> {
        loop {
            match reader.read_event(buf) {
                Ok(Event::Start(ref e)) => {
                    if let (XliffMajorVersion::XliffMajorV1, b"trans-unit")
                    | (XliffMajorVersion::XliffMajorV2, b"unit") = (self.major, e.name())
                    {
                        self.body
                            .extend(TransUnit::parse_from_entity(reader, self.major, e)?);
                    }
                }
                Ok(Event::End(ref e)) => {
                    if let (XliffMajorVersion::XliffMajorV1, b"body")
                    | (XliffMajorVersion::XliffMajorV2, b"file") = (self.major, e.name())
                    {
                        break;
                    }
                }
                Ok(Event::Eof) => break,
                Err(e) => return Err(error::Error::from(e)),
                _ => (),
            }
        }

        Ok(())
    }

    /// An XLIFF body parser from a XML event reader.
    ///
    /// Borrows a mutable `quick_xml::Reader` and modifies the `XliffDoc`
    /// accourdingly.
    ///
    /// # Errors
    ///
    /// Returns `xliffication::error::Error`s on invalid XML input.
    pub fn parse_body<R: Read>(
        &mut self,
        reader: &mut Reader<BufReader<R>>,
    ) -> Result<(), error::Error> {
        let mut buf = Vec::new();

        loop {
            match reader.read_event(&mut buf) {
                Ok(Event::Start(ref e)) => {
                    if let b"body" = e.name() {
                        let mut body = Vec::new();
                        self.parse_translation_units(reader, &mut body)?;
                        break;
                    }
                }
                Ok(Event::Eof) => break,
                Err(e) => return Err(error::Error::from(e)),
                _ => (),
            }
        }

        Ok(())
    }

    pub fn sort_body(&mut self) {
        self.body.sort_by(|ctx_a, ctx_b| {
            let a = ctx_a.ctx_group().as_ref().map_or(0, |c| c.line_number);
            let b = ctx_b.ctx_group().as_ref().map_or(0, |c| c.line_number);
            a.cmp(&b)
        });
    }

    /// An XLIFF file parser from a XML event reader.
    ///
    /// Borrows a mutable `quick_xml::Reader` and modifies the `XliffDoc`
    /// accourdingly.
    ///
    /// # Errors
    ///
    /// Returns `xliffication::error::Error`s on invalid XML input.
    pub fn parse_file<R: Read>(
        &mut self,
        reader: &mut Reader<BufReader<R>>,
    ) -> Result<(), error::Error> {
        let mut buf = Vec::new();

        loop {
            match reader.read_event(&mut buf) {
                Ok(Event::Start(ref e)) => {
                    if let b"file" = e.name() {
                        for attr in e.attributes() {
                            /* match self.version {
                                XliffVersion::XliffV1_0
                                | XliffVersion::XliffV1_1
                                | XliffVersion::XliffV1_2 => match attr {
                                    Ok(a) => match a.key {
                                        b"source-language" => self.owned_source(a.value.to_vec()),
                                        b"target-language" => self.owned_target(a.value.to_vec()),
                                        _ => (),
                                    },
                                    Err(e) => {
                                        return Err(error::Error::from(e));
                                    }
                                },
                                XliffVersion::XliffV2_0 | XliffVersion::XliffV2_1 => match attr {
                                    Ok(a) => match a.key {
                                        b"srcLang" => self.owned_source(a.value.to_vec()),
                                        b"trgLang" => self.owned_target(a.value.to_vec()),
                                        _ => (),
                                    },
                                    Err(e) => {
                                        return Err(error::Error::from(e));
                                    }
                                },
                                XliffVersion::Invalid => (),
                            } */
                            match attr {
                                Ok(a) => match (self.major, a.key) {
                                    (XliffMajorVersion::XliffMajorV1, b"source-language")
                                    | (XliffMajorVersion::XliffMajorV2, b"srcLang") => {
                                        self.owned_source(a.value.to_vec());
                                    }
                                    (XliffMajorVersion::XliffMajorV1, b"target-language")
                                    | (XliffMajorVersion::XliffMajorV2, b"trgLang") => {
                                        self.owned_target(a.value.to_vec());
                                    }
                                    _ => (),
                                },
                                Err(e) => {
                                    return Err(error::Error::from(e));
                                }
                            }
                            /* match reader.read_event(&mut buf) {
                                Ok(Event::Start(ref e)) => {
                                    if let b"body" = e.name() {
                                    }
                                }
                                Ok(Event::Eof) => {
                                    return Err(quick_xml::Error::from(io::Error::from(
                                        io::ErrorKind::UnexpectedEof,
                                    )))
                                }
                                Err(e) => return Err(e),
                                _ => (),
                            } */
                        }
                        match self.major {
                            XliffMajorVersion::XliffMajorV1 => self.parse_body(reader)?,
                            XliffMajorVersion::XliffMajorV2 => {
                                let mut body = Vec::new();
                                self.parse_translation_units(reader, &mut body)?;
                            }
                            XliffMajorVersion::Invalid => (),
                        }
                    }
                }
                Ok(Event::End(ref e)) => {
                    if let (XliffMajorVersion::XliffMajorV1, b"file")
                    | (XliffMajorVersion::XliffMajorV2, b"xliff") = (self.major, e.name())
                    {
                        break;
                    }
                }
                Ok(Event::Eof) => break,
                Err(e) => return Err(error::Error::from(e)),
                _ => (),
            }
        }

        Ok(())
    }

    /// An XLIFF document parser from a XML event reader.
    ///
    /// Borrows a mutable `quick_xml::Reader` and modifies the `XliffDoc`
    /// accourdingly.
    ///
    /// # Errors
    ///
    /// Returns `xliffication::error::Error`s on invalid XML input.
    pub fn parse_doc<R: Read>(
        &mut self,
        reader: &mut Reader<BufReader<R>>,
    ) -> Result<(), error::Error> {
        let mut buf = Vec::new();

        loop {
            match reader.read_event(&mut buf) {
                Ok(Event::Start(ref e)) => {
                    if let b"xliff" = e.name() {
                        for attr in e.attributes() {
                            match attr {
                                Ok(a) => match a.key {
                                    b"version" => self.parse_version(a.value.as_ref())?,
                                    _ => (),
                                },
                                Err(e) => {
                                    return Err(error::Error::from(e));
                                }
                            }
                        }
                        self.parse_file(reader)?;
                    }
                }
                Ok(Event::End(ref e)) => {
                    if let (XliffMajorVersion::XliffMajorV1, b"xliff") = (self.major, e.name()) {
                        break;
                    }
                }
                // Ok(Event::Text(e)) => break,
                Ok(Event::Eof) => {
                    if let XliffMajorVersion::XliffMajorV2 = self.major {
                        break;
                    }
                    return Err(error::Error::from(io::Error::from(
                        io::ErrorKind::UnexpectedEof,
                    )));
                }
                Err(e) => return Err(error::Error::from(e)),
                _ => (),
            }
        }

        Ok(())
    }
}

impl<'a> Default for XliffDoc<'a> {
    fn default() -> Self {
        XliffDoc::new()
    }
}
