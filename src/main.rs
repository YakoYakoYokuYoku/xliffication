use clap::{App, Arg};
use quick_xml::Reader;

use std::fs::File;
use std::io::BufReader;

use xliffication::body::XliffDoc;
use xliffication::replace::Replacer;

fn main() {
    let matches = App::new("XliffIcation")
        .version("0.0.0")
        .author("Martin Reboredo <yakoyoku@gmail.com>")
        .about("XliffIcation takes XLIFF files as input and outputs the best applied translations.")
        .arg(
            Arg::with_name("destdir")
                .short("d")
                .long("destdir")
                .value_name("DEST")
                .help("Sets a destination dir")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("INPUT")
                .help("Sets the input XLIFF file to use")
                .required(true)
                .index(1),
        )
        .get_matches();

    let xlf_file = matches.value_of("INPUT").unwrap();
    let destdir = matches.value_of("destdir").unwrap_or("./out");

    let xliff_inp = match File::open(xlf_file) {
        Ok(xlf) => xlf,
        Err(e) => {
            eprintln!("couldn't open {}: {}", xlf_file, e);
            return;
        }
    };
    let mut xlf_reader = Reader::from_reader(BufReader::new(xliff_inp));
    xlf_reader.trim_text(true);

    let mut doc = XliffDoc::new();
    doc.parse_doc(&mut xlf_reader).unwrap();
    doc.sort_body();
    let replacer = Replacer::<File>::new(doc, String::from(destdir));
    replacer.replace_strings().unwrap();
}
