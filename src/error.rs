use quick_xml;

use std::error;
use std::fmt::{self, Display, Formatter};
use std::io;
use std::str;
use std::string;

#[derive(Debug)]
pub enum Error {
    Io(io::Error),
    Utf8Error(str::Utf8Error),
    FromUtf8Error(string::FromUtf8Error),
    XmlError(quick_xml::Error),
    XliffError(String),
    UnexpectedEof,
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Error::Io(e)
    }
}

impl From<str::Utf8Error> for Error {
    fn from(e: str::Utf8Error) -> Self {
        Error::Utf8Error(e)
    }
}

impl From<string::FromUtf8Error> for Error {
    fn from(e: string::FromUtf8Error) -> Self {
        Error::FromUtf8Error(e)
    }
}

impl From<quick_xml::Error> for Error {
    fn from(e: quick_xml::Error) -> Self {
        Error::XmlError(e)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Error::Io(e) => write!(f, "I/O error: {}", e),
            Error::Utf8Error(e) => write!(f, "UTF-8 error: {}", e),
            Error::FromUtf8Error(e) => write!(f, "UTF-8 error: {}", e),
            Error::XmlError(e) => write!(f, "XML error: {}", e),
            Error::XliffError(e) => write!(f, "XLIFF error: {}", e),
            Error::UnexpectedEof => write!(f, "Unexpected EOF"),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            Error::Io(e) => Some(e),
            Error::Utf8Error(e) => Some(e),
            Error::FromUtf8Error(e) => Some(e),
            Error::XmlError(e) => Some(e),
            Error::XliffError(_) | Error::UnexpectedEof => None,
        }
    }
}
