use std::collections::hash_map::{Entry, HashMap};
use std::fs::{self, File};
use std::io::{self, prelude::*, BufReader, LineWriter};
use std::path::Path;
use std::str;

use crate::body::XliffDoc;
use crate::error;

struct FileReplacer<F: Read + Write> {
    read_from: BufReader<F>,
    write_to: LineWriter<F>,
    current_line: u32,
}

pub struct Replacer<'a, F: Read + Write> {
    doc: XliffDoc<'a>,
    dest: String,
    file_map: HashMap<Vec<u8>, FileReplacer<F>>,
}

impl<'a, F: Read + Write + ReplacerIoSystem<F>> Replacer<'a, F> {
    #[must_use]
    pub fn new(doc: XliffDoc<'a>, dest: String) -> Self {
        let file_map = HashMap::<Vec<u8>, FileReplacer<F>>::new();
        Replacer {
            doc,
            dest,
            file_map,
        }
    }

    /// An XLIFF translation merger.
    ///
    /// Consumes an `XliffDoc` and performs merges across the input files.
    ///
    /// # Errors
    ///
    /// Returns `xliffication::error::Error` on an invalid `XliffDoc` input.
    #[allow(clippy::too_many_lines)]
    pub fn replace_strings(self) -> Result<(), error::Error> {
        let mut file_map = HashMap::<Vec<u8>, FileReplacer<F>>::new();
        let target_lang = Vec::from(self.doc.target());
        let mut dest = self.dest;
        dest.extend("/".chars());
        dest.extend(String::from_utf8(target_lang.clone())?.chars());
        dest.extend("/".chars());
        if self.file_map.is_empty() {
            F::create_dir(&dest)?;
        }

        for tlunit in self.doc.body() {
            if !tlunit.is_approved() {
                continue;
            }
            let source = tlunit.source();
            let target = tlunit.target();
            let source_line_count = tlunit.source_line_count() as usize;
            let target_line_count = tlunit.target_line_count() as usize;
            let ctx_group = if let Some(ctx) = tlunit.ctx_group() {
                ctx
            } else {
                continue;
            };

            let line_number = ctx_group.line_number;
            let path = Vec::from(ctx_group.sourcefile().as_ref());
            let replacer = match file_map.entry(path.clone()) {
                Entry::Occupied(e) => e.into_mut(),
                Entry::Vacant(e) => {
                    let mut dest = dest.clone();
                    dest += str::from_utf8(&path)?;

                    let input = String::from_utf8(path)?;
                    let output = dest;

                    let replacer = FileReplacer {
                        read_from: BufReader::new(F::open(input)?),
                        write_to: LineWriter::new(F::create(output)?),
                        current_line: 0,
                    };

                    e.insert(replacer)
                }
            };

            if line_number > 0 {
                let reader = &mut replacer.read_from;
                let writer = &mut replacer.write_to;
                let mut input_lines = reader.by_ref().lines();
                let line_count = (line_number - replacer.current_line) as usize;

                for line in input_lines.by_ref().take(line_count - 1) {
                    writer.write_all(line?.as_bytes())?;
                    writer.write_all(b"\n")?;
                }

                let mut line = String::new();
                let src = str::from_utf8(&source)?;
                for l in input_lines.take(source_line_count + 1) {
                    line += &(l.unwrap_or_else(|_| String::new()) + "\n");
                }
                line.pop();

                match line.find(src) {
                    Some(p) => {
                        let mut begin_lines = line.as_bytes()[..p].lines();
                        let begin_line_count = line.as_bytes()[..p].iter().fold(0, |sum, &b| {
                            if b == b'\n' {
                                sum + 1
                            } else {
                                sum
                            }
                        });
                        for i in begin_lines.by_ref().take(begin_line_count) {
                            writer.write_all(i?.as_bytes())?;
                            writer.write_all(b"\n")?;
                        }
                        writer.write_all(
                            begin_lines
                                .next()
                                .unwrap_or_else(|| Ok(String::new()))?
                                .as_bytes(),
                        )?;

                        let mut middle_lines = target.lines();
                        for o in middle_lines.by_ref().take(target_line_count) {
                            writer.write_all(o?.as_bytes())?;
                            writer.write_all(b"\n")?;
                        }
                        writer.write_all(
                            middle_lines
                                .next()
                                .unwrap_or_else(|| Ok(String::new()))?
                                .as_bytes(),
                        )?;

                        let mut end_lines = line.as_bytes()[(p + src.len())..].lines();
                        let end_line_count = line.as_bytes()[(p + src.len())..]
                            .iter()
                            .fold(0, |sum, &b| if b == b'\n' { sum + 1 } else { sum });
                        for i in end_lines.by_ref().take(end_line_count) {
                            writer.write_all(i?.as_bytes())?;
                            writer.write_all(b"\n")?;
                        }
                        writer.write_all(
                            end_lines
                                .next()
                                .unwrap_or_else(|| Ok(String::new()))?
                                .as_bytes(),
                        )?;
                    }
                    None => writer.write_all(line.as_bytes())?,
                }
                writer.write_all(b"\n")?;
                replacer.current_line = line_number;
            }
        }

        for (_, mut replacer) in file_map {
            let mut buf = Vec::new();
            replacer.read_from.read_to_end(&mut buf)?;
            replacer.write_to.write_all(&buf)?;
            replacer.write_to.flush()?;
        }

        Ok(())
    }
}

/// The `ReplacerIoSystem<F>` trait implements an input and output system intended for replacements.
///
/// To maintain independency from being reliant on File I/O the trait provides filesystem methods for either use of a real one
/// or a mock one. Useful for scenarios such as browsers and testing.
///
/// Although this trait is intended for usage with `Replacer`.
pub trait ReplacerIoSystem<F: Read + Write> {
    /// Creates an output directory intended for the merged translations. If the directory was already existent it won't
    /// return error.
    ///
    /// # Errors
    ///
    /// Returns `io::Error` either on permission or the path not being a directory errors.
    fn create_dir<P: AsRef<Path>>(path: P) -> io::Result<()>;

    /// Creates an output file that contains merged translations. If the file was already existent it's going to be trucated.
    ///
    /// # Errors
    ///
    /// Returns `io::Error` either on permission or the path being a directory errors.
    fn create<P: AsRef<Path>>(path: P) -> io::Result<F>;

    /// Opens an input file that contains the source strings.
    ///
    /// # Errors
    ///
    /// Returns `io::Error` either on permission or the path being a directory errors.
    fn open<P: AsRef<Path>>(path: P) -> io::Result<F>;
}

impl ReplacerIoSystem<File> for File {
    fn create_dir<P: AsRef<Path>>(path: P) -> io::Result<()> {
        fs::create_dir_all(&path)
    }

    fn create<P: AsRef<Path>>(path: P) -> io::Result<File> {
        File::create(&path)
    }

    fn open<P: AsRef<Path>>(path: P) -> io::Result<File> {
        File::open(&path)
    }
}
