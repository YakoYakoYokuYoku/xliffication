use quick_xml::events::{BytesStart, Event};
use quick_xml::Reader;

use std::borrow::Cow;
use std::io::{self, BufReader, Read};
use std::str;

use crate::body::XliffMajorVersion;
use crate::context::ContextGroup;
use crate::error;

#[derive(Clone, Debug)]
pub struct TransUnit<'a> {
    id: i32,
    source_line_count: u32,
    target_line_count: u32,
    approved: bool,
    source: Cow<'a, [u8]>,
    target: Cow<'a, [u8]>,
    ctx_group: Option<ContextGroup<'a>>,
}

impl<'a> TransUnit<'a> {
    #[must_use]
    pub fn new() -> Self {
        TransUnit {
            id: -1,
            source_line_count: 0,
            target_line_count: 0,
            approved: false,
            source: Cow::default(),
            target: Cow::default(),
            ctx_group: None,
        }
    }

    #[must_use]
    pub fn is_approved(&self) -> bool {
        self.approved
    }

    #[must_use]
    pub fn source_line_count(&self) -> u32 {
        self.source_line_count
    }

    #[must_use]
    pub fn target_line_count(&self) -> u32 {
        self.target_line_count
    }

    #[must_use]
    pub fn source(&'a self) -> Cow<'a, [u8]> {
        Cow::Borrowed(&*self.source)
    }

    #[must_use]
    pub fn target(&'a self) -> Cow<'a, [u8]> {
        Cow::Borrowed(&*self.target)
    }

    #[must_use]
    pub fn ctx_group(&'a self) -> &'a Option<ContextGroup<'a>> {
        &self.ctx_group
    }

    /// An XLIFF translation unit parser from a XML event reader.
    ///
    /// Consumes a `quick_xml::Reader` with a `quick_xml::BytesStart` and returns a vector of
    /// `TransUnit`s.
    ///
    /// # Errors
    ///
    /// Returns `xliffication::error::Error`s on invalid XML input.
    pub fn parse_from_entity<R: Read>(
        reader: &mut Reader<BufReader<R>>,
        major: XliffMajorVersion,
        e: &BytesStart,
    ) -> Result<Vec<Self>, error::Error> {
        let mut buf = Vec::new();
        let mut ret = Vec::new();
        let mut tlunit = Self::new();

        for attr in e.attributes() {
            match attr {
                Ok(a) => match a.key {
                    b"id" => {
                        tlunit.id = match str::from_utf8(a.value.as_ref())?.parse() {
                            Ok(id) => id,
                            Err(_) => {
                                return Err(error::Error::from(io::Error::from(
                                    io::ErrorKind::InvalidInput,
                                )))
                            }
                        };
                    }
                    b"approved" => match a.value.as_ref() {
                        b"yes" => {
                            tlunit.approved = true;
                        }
                        b"no" => {
                            tlunit.approved = false;
                        }
                        _ => {
                            return Err(error::Error::from(io::Error::from(
                                io::ErrorKind::InvalidInput,
                            )))
                        }
                    },
                    _ => (),
                },
                Err(e) => return Err(error::Error::from(e)),
            }
        }
        loop {
            match reader.read_event(&mut buf) {
                Ok(Event::Start(ref e)) => match e.name() {
                    b"source" => {
                        let mut source = Vec::new();
                        let original = Vec::from(reader.read_text(e.name(), &mut source)?);
                        tlunit.source_line_count =
                            original
                                .iter()
                                .fold(0, |sum, &b| if b == b'\n' { sum + 1 } else { sum });
                        tlunit.source = Cow::Owned(original);
                    }
                    b"target" => {
                        for attr in e.attributes() {
                            match attr {
                                Ok(a) => {
                                    if let b"translated" = a.key {
                                        println!("{}", str::from_utf8(a.value.as_ref())?);
                                    }
                                }
                                Err(e) => {
                                    eprintln!("couldn't parse attribute in test.xlf: {}", e);
                                    return Err(error::Error::from(e));
                                }
                            }
                        }
                        let mut target = Vec::new();
                        let translation = Vec::from(reader.read_text(e.name(), &mut target)?);
                        tlunit.target_line_count =
                            translation
                                .iter()
                                .fold(0, |sum, &b| if b == b'\n' { sum + 1 } else { sum });
                        tlunit.target = Cow::Owned(translation);
                    }
                    b"context-group" => {
                        if tlunit.ctx_group().is_some() {
                            let prev = tlunit;
                            tlunit = Self::new();
                            tlunit.approved = prev.approved;
                            tlunit.id = prev.id;
                            tlunit.source = prev.source.clone();
                            tlunit.target = prev.target.clone();
                            ret.push(prev);
                        }
                        tlunit.ctx_group = Some(ContextGroup::parse_from_entity(reader, e)?);
                    }
                    _ => (),
                },
                Ok(Event::End(ref e)) => {
                    if let (XliffMajorVersion::XliffMajorV1, b"trans-unit")
                    | (XliffMajorVersion::XliffMajorV2, b"unit") = (major, e.name())
                    {
                        break;
                    }
                }
                Ok(Event::Eof) => {
                    return Err(error::Error::from(io::Error::from(
                        io::ErrorKind::UnexpectedEof,
                    )))
                }
                Err(e) => return Err(error::Error::from(e)),
                _ => (),
            }
        }
        ret.push(tlunit);
        /* println!(
            "trans-unit id: {}, approved: {}, source: {}, target: {}, file: {}\n---\n",
            tlunit.id,
            tlunit.approved,
            str::from_utf8(&tlunit.source).unwrap(),
            str::from_utf8(&tlunit.target).unwrap(),
            str::from_utf8(
                &tlunit
                    .ctx_group()
                    .as_ref()
                    .unwrap_or(&ContextGroup::default())
                    .sourcefile()
            )
            .unwrap()
        ); */
        Ok(ret)
    }
}

impl<'a> Default for TransUnit<'a> {
    fn default() -> Self {
        TransUnit::new()
    }
}
