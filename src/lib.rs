#![deny(clippy::pedantic)]
#![allow(clippy::module_name_repetitions)]

pub mod body;
pub mod context;
pub mod error;
pub mod replace;
pub mod transunit;

pub use body::{XliffDoc, XliffMajorVersion, XliffVersion};
pub use error::Error;
